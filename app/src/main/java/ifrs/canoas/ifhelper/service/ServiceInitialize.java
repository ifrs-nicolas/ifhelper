package ifrs.canoas.ifhelper.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ServiceInitialize extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("TAG_IFHELPER", "SERVICE - ServiceInitialize onReceive()");
        startService(context, ScrapsService.class);
    }

    private static void startService(Context context, Class<?> cls) {
        Intent service = new Intent(context, cls);
        context.stopService(service);
//      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//          ContextCompat.startForegroundService(context, service);
//      else
            context.startService(service);
    }
}
