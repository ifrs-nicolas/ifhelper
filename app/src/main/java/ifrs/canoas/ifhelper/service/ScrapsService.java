package ifrs.canoas.ifhelper.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ifrs.canoas.ifhelper.model.Message;
import ifrs.canoas.ifhelper.model.MessageWrapper;
import ifrs.canoas.ifhelper.model.User;
import ifrs.canoas.ifhelper.ui.MessagesListActivity;
import ifrs.canoas.ifhelper.util.AppUtils;
import ifrs.canoas.ifhelper.util.NotificationHelper;
import ifrs.canoas.ifhelper.webservice.Moodle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrapsService extends Service {

    private Timer timerActual = new Timer();
    protected TimerTask task;
    private final Handler handler = new Handler();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.v("TAG_IFHELPER", "SERVICE - onCreate()");
        super.onCreate();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.i("TAG_IFHELPER", "onStartCommand()");
        task = new TimerTask() {
            public void run() {
                handler.post(() ->
                        getMessagesFromWSAndNotifyWhenNews()
                );
            }
        };
        //timerActual.schedule(task, 30000, 3600000);
        timerActual.schedule(task, 30000, 60000);
        return (super.onStartCommand(intent, flags, startId));//Continua ciclo de vida do meu serviço
    }

    private void getMessagesFromWSAndNotifyWhenNews() {
        SharedPreferences sharedPref = getSharedPreferences("moodle_messages", Context.MODE_PRIVATE);
        boolean wifi = AppUtils.isConnectedWifi(getApplicationContext());
        int attempts = sharedPref.getInt("attempts", 0);
        Log.d("MOODLE_MESSAGES", "WIFI " + wifi);
        Log.d("MOODLE_MESSAGES", "ATTEMPTS " + attempts);
        if (!wifi) {
            attempts += 1;
            sharedPref.edit().putInt("attempts", attempts).apply();
        }
        if (wifi || attempts >= 60) {
            attempts = 0;
            sharedPref.edit().putInt("attempts", attempts).apply();
            User user = AppUtils.getUser();
            if (user != null && user.getToken() != null && user.getUserid() != null) {
                Moodle.getCoursesWS().getMessages(user.getToken(), user.getUserid()).enqueue(new Callback<MessageWrapper>() {
                    public void onResponse(@NonNull Call<MessageWrapper> call, @NonNull Response<MessageWrapper> response) {
                        if (response.body() != null && "invalidtoken".equals(response.body().getErrorcode())) {
                            AppUtils.removeUser();
                        } else {
                            List<Message> list = response.body() == null || response.body().getMessages() == null ? new ArrayList<>() : response.body().getMessages();

                            int messagesSizeOld = sharedPref.getInt("messages_size", 0);
                            int messagesSizeNew = list.size();

                            Log.d("MOODLE_MESSAGES", "OLD " + messagesSizeOld);
                            Log.d("MOODLE_MESSAGES", "NEW " + messagesSizeNew);
                            if (messagesSizeOld < messagesSizeNew) {
                                sharedPref.edit().putInt("messages_size", messagesSizeNew).apply();
                                String title = "IFHELPER";
                                String message = (messagesSizeNew - messagesSizeOld) + " novas mensagens no Moodle";
                                new NotificationHelper(getApplicationContext(), MessagesListActivity.class)
                                        .createNotification(title, message);
                            }
                        }
                    }

                    public void onFailure(@NonNull Call<MessageWrapper> call, @NonNull Throwable t) {
                        Log.e("Error MoodleAPI", t.getMessage());
                    }
                });
            }
        }
    }
}
