package ifrs.canoas.ifhelper.viewmodel;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.dao.AppDatabase;
import ifrs.canoas.ifhelper.dao.NoteDao;
import ifrs.canoas.ifhelper.model.Note;
import ifrs.canoas.ifhelper.util.AppUtils;
import ifrs.canoas.ifhelper.util.AsyncLiveData;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CourseDataViewModel extends ViewModel {

    private final NoteDao noteDao;
    private Integer courseId;
    private List<Integer> checkedIds;
    private AsyncLiveData<Note> note;
    private AsyncLiveData<List<Note>> notes;
    private AsyncLiveData<String> alert;

    public CourseDataViewModel() {
        this.noteDao = AppDatabase.getInstance().noteDao();
    }

    public void init(int courseId) {
        this.courseId = courseId;
        this.checkedIds = new ArrayList<>();
        this.note = new AsyncLiveData<>(new Note());
        this.notes = new AsyncLiveData<>(new ArrayList<>());
        this.alert = new AsyncLiveData<>("");
        this.refreshList(this.courseId);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("CheckResult")
    private void refreshList(final int courseId) {
        Single.fromCallable(() -> {
            List<Note> notesInDB = noteDao.loadByCourseId(courseId);
            notes.postValue(notesInDB);
            for (Note item : notesInDB) {
                if (item.getImage() != null) {
                    try {
                        Bitmap bm = AppUtils.getBitmapFromImagesFolder(item.getImage());
                        item.setBmImage(Bitmap.createScaledBitmap(bm, 300, 300, true));
                    } catch (Exception e) {
                        item.setImage(null);
                        item.setNote(String.format("Imagem não encontrada no dispositivo. %s", item.getNote()));
                    }
                }
            }
            return notesInDB;
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(notes::postValue);
    }

    public void editNote(final Integer id) {
        Single.fromCallable(() -> noteDao.loadById(id)).doOnSuccess(note::setValue).subscribe();
    }

    public void saveNote(final String text) {
        if (Strings.isNullOrEmpty(text))
            alert.setValue(String.format("%s - %s.",
                    AppUtils.getMessage(R.string.error_note_save), AppUtils.getMessage(R.string.error_note_empty)));
        else {
            Note temp = this.note == null || this.note.getValue() == null ?
                    new Note() : this.note.getValue();
            if (temp.getId() == null) temp.setCreatedDate(Calendar.getInstance().getTime());
            temp.setCourseId(courseId);
            temp.setNote(text);
            Single.fromCallable(() -> {
                noteDao.insert(temp);
                return true;
            }).doOnSuccess(result -> {
                note.setValue(new Note());
                alert.setValue(AppUtils.getMessage(R.string.note_success_save));
                this.refreshList(this.courseId);
            }).subscribe();
        }
    }

    public void saveNote(final Note noteTemp) {
        Single.fromCallable(() -> {
            noteDao.insert(noteTemp);
            return true;
        }).doOnSuccess(result -> {
            alert.setValue(AppUtils.getMessage(R.string.note_success_save));
            this.refreshList(this.courseId);
        }).subscribe();
    }

    public void deleteNotes() {
        final List<Integer> selectedNotesIds = this.getCheckedIds();
        Single.fromCallable(() -> {
            List<Note> notes = noteDao.loadByIds(selectedNotesIds);
            for (Note note : notes) {
                if (note.getImage() != null) {
                    boolean deleted = AppUtils.getFileFromImagesFolder(note.getImage()).getCanonicalFile().delete();
                    Log.d("CourseDataVM", "previous image deleted: " + deleted);
                }
            }
            noteDao.delete(notes);
            return true;
        }).doOnSuccess(result -> {
            checkedIds.clear();
            alert.setValue(AppUtils.getMessage(R.string.note_delete_success));
            this.refreshList(this.courseId);
        }).doOnError(throwable ->
                alert.setValue(AppUtils.getMessage(R.string.error_note_delete, throwable)
                )
        ).subscribe();
    }

    public LiveData<Note> getNote() {
        return note;
    }

    public LiveData<List<Note>> getNotes() {
        return notes;
    }

    public List<Integer> getCheckedIds() {
        return checkedIds;
    }

    public void setCheckedIds(List<Integer> checkedIds) {
        this.checkedIds = checkedIds;
    }

    public MutableLiveData<String> getAlert() {
        return alert;
    }
}
