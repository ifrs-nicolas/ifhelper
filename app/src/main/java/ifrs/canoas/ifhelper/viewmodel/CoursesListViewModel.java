package ifrs.canoas.ifhelper.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ifrs.canoas.ifhelper.dao.AppDatabase;
import ifrs.canoas.ifhelper.dao.CourseDao;
import ifrs.canoas.ifhelper.model.Course;
import ifrs.canoas.ifhelper.model.User;
import ifrs.canoas.ifhelper.util.AppUtils;
import ifrs.canoas.ifhelper.util.AsyncLiveData;
import ifrs.canoas.ifhelper.webservice.Moodle;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoursesListViewModel extends ViewModel {

    private User user;
    private final CourseDao courseDao;
    private AsyncLiveData<List<Course>> courses;

    public CoursesListViewModel() {
        this.courseDao = AppDatabase.getInstance().courseDao();
    }

    public void init(User user) {
        this.user = user;
        this.courses = new AsyncLiveData<>();
        getCoursesFromWS();
    }

    private void getCoursesFromDB() {
        Single.fromCallable(() -> courseDao.getAllByUser(user.getUserid())).doOnSuccess(courses::setValue).subscribe();
    }

    private void getCoursesFromWS() {
        Moodle.getCoursesWS().getCoursesByUser(user.getToken(), user.getUserid()).enqueue(new Callback<List<Course>>() {
            @Override
            public void onResponse(@NonNull Call<List<Course>> call, @NonNull Response<List<Course>> response) {
                List<Course> coursesWS = response.body();
                if (coursesWS == null) {
                    courses.setValue(new ArrayList<>());
                } else {
                    for (Course course : coursesWS) course.setUserId(user.getUserid());
                    courses.setValue(coursesWS);
                    AsyncTask.execute(() -> courseDao.insertAll(coursesWS));
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<Course>> call, @NonNull Throwable t) {
                Log.e("ERROR IFHELPER", t.getMessage());
                Log.e("ERROR IFHELPER", t.toString());
                getCoursesFromDB();
            }
        });
    }

    public LiveData<List<Course>> getCourses() {
        return courses;
    }
}
