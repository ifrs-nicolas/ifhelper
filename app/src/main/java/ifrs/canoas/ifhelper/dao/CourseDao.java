package ifrs.canoas.ifhelper.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ifrs.canoas.ifhelper.model.Course;

@Dao
public interface CourseDao {

    @Query("SELECT * FROM Course WHERE user_id = :userId")
    List<Course> getAllByUser(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Course> courses);

}
