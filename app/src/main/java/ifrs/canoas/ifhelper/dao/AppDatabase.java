package ifrs.canoas.ifhelper.dao;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import ifrs.canoas.ifhelper.App;
import ifrs.canoas.ifhelper.model.Course;
import ifrs.canoas.ifhelper.model.Note;

@Database(entities = {Course.class, Note.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static final String DB_NAME = "database-ifhelper";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance() {
        return getInstance(App.getContext());
    }

    private static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DB_NAME)
                .allowMainThreadQueries()
                //.fallbackToDestructiveMigration()
                .addMigrations(MIGRATION_2_3)
                .build();
    }

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE note ADD COLUMN image VARCHAR");
        }
    };

    public abstract CourseDao courseDao();
    public abstract NoteDao noteDao();
}

