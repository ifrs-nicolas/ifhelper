package ifrs.canoas.ifhelper.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ifrs.canoas.ifhelper.model.Note;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM Note WHERE course_id = :courseId")
    List<Note> loadByCourseId(Integer courseId);

    @Query("SELECT * FROM Note WHERE id = :id")
    Note loadById(int id);

    @Query("SELECT * FROM Note WHERE id IN (:ids)")
    List<Note> loadByIds(List<Integer> ids);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Note course);

    @Delete
    void delete(List<Note> notes);
}
