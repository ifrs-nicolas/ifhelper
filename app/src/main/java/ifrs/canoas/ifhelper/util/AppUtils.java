package ifrs.canoas.ifhelper.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ifrs.canoas.ifhelper.App;
import ifrs.canoas.ifhelper.CONST;
import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.User;

public class AppUtils {

    public static User getUser() {
        User user = null;
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(App.getContext().getString(R.string.user), Context.MODE_PRIVATE);
        String token = sharedPref.getString("token", null);
        if (token != null) {
            user = new User();
            user.setToken(token);
            user.setUserid(sharedPref.getInt("userId", 0));
            user.setFullname(sharedPref.getString("fullname", null));
            user.setUsername(sharedPref.getString("username", null));
        }
        return user;
    }

    public static void removeUser() {
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(App.getContext().getString(R.string.user), Context.MODE_PRIVATE);
        sharedPref.edit().clear().apply();
    }

    @SuppressLint("SimpleDateFormat")
    public static String brDate(Date date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getMessage(int id) {
        return App.getContext().getResources().getString(id);
    }

    public static String getMessage(int id, Throwable throwable) {
        return String.format("%s %s", App.getContext().getResources().getString(id), throwable.getLocalizedMessage());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static File getSdFolder(String folder) {
        File sd = new File(Environment.getExternalStorageDirectory(), folder);
        if (!sd.exists()) sd.mkdirs();
        return sd;
    }

    public static Uri getFileURI(Context context, File file) {
        return FileProvider.getUriForFile(context, CONST.FILE_PROVIDER_AUTHORITY, file);
    }

    public static File getFileFromImagesFolder(String relativePathOfFile) {
        return new File(getSdFolder(CONST.APP_IMAGES_FOLDER), relativePathOfFile);
    }

    public static Bitmap getBitmapFromImagesFolder(String relativePathOfFile) {
        return BitmapFactory.decodeFile(getFileFromImagesFolder(relativePathOfFile).getAbsolutePath());
    }

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String getUrlFromString(String string) {
        Matcher matcher = urlPattern.matcher(string);
        String url = null;
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            url = string.substring(matchStart, matchEnd);
        }
        return url;
    }

    public static boolean isConnectedWifi(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                return (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI);
            }
        }
        return false;
    }

    /* public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    } */
}