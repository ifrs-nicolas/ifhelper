package ifrs.canoas.ifhelper.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Note;
import ifrs.canoas.ifhelper.util.AppUtils;

public class NotesListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private ArrayList<Note> originalData;
    private List<Integer> checkedIds = new ArrayList<>();
    private AdapterView.OnItemClickListener onItemClickListener;
    private AdapterView.OnItemLongClickListener onItemLongClickListener;

    public NotesListAdapter(List<Note> list, Context context) {
        this.originalData = list == null ? new ArrayList<>() : new ArrayList<>(list);
        this.mInflater = LayoutInflater.from(context);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint({"ViewHolder", "CheckResult", "InflateParams"})
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = mInflater.inflate(R.layout.item_note, null);
        TextView note = view.findViewById(R.id.note);
        TextView date = view.findViewById(R.id.create_date);
        ImageView image = view.findViewById(R.id.noteImageView);
        CheckBox checkBox = view.findViewById(R.id.noteCheckBox);

        Note item = originalData.get(position);
        note.setText(item.getNote());
        date.setText(AppUtils.brDate(item.getCreatedDate()));
        checkBox.setChecked(checkedIds.contains(item.getId()));

        if (item.getImage() != null) {
            if (item.getBmImage() != null) {
                image.setImageBitmap(item.getBmImage());
                image.setVisibility(View.VISIBLE);
            } else {
                note.append(" Carregando imagem anexada...");
            }
        }

        checkBox.setOnClickListener(v -> checkClick(position, v, parent, item, checkBox));
        view.setOnClickListener(v -> checkClick(position, v, parent, item, checkBox));

        if (this.onItemLongClickListener != null)
            view.setOnLongClickListener(v -> this.onItemLongClickListener.onItemLongClick((AdapterView) parent, v, position, item.getId()));

        return view;
    }

    private void checkClick(int position, View v, ViewGroup parent, Note item, CheckBox checkBox) {
        if (checkedIds.contains(item.getId())) {
            checkedIds.remove(item.getId());
            checkBox.setChecked(false);
        } else {
            checkedIds.add(item.getId());
            checkBox.setChecked(true);
        }
        if (this.onItemClickListener != null)
            this.onItemClickListener.onItemClick((AdapterView) parent, v, position, item.getId());
    }

    @Override
    public int getCount() {
        return this.originalData.size();
    }

    @Override
    public Object getItem(int position) {
        return this.originalData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.originalData.get(position).getId();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public List<Integer> getCheckedIds() {
        return checkedIds;
    }

    public void setCheckedIds(List<Integer> checkedIds) {
        this.checkedIds = checkedIds;
    }
}
