package ifrs.canoas.ifhelper.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.dao.AppDatabase;
import ifrs.canoas.ifhelper.dao.NoteDao;
import ifrs.canoas.ifhelper.model.Note;
import ifrs.canoas.ifhelper.ui.abstracts.PrivateActivity;
import ifrs.canoas.ifhelper.util.AppUtils;
import ifrs.canoas.ifhelper.util.CheckPermissionUtil;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TakePictureActivity extends PrivateActivity {
    private final static int CAMERA_REQUEST = 1;//Constante para request code
    private String mCurrentPhotoPath = null;
    private Note note;
    private NoteDao noteDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_note_picture);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentReturn) {
        if (intentReturn != null) {
            Bundle bundle = intentReturn.getExtras();
            if (bundle != null && requestCode == CAMERA_REQUEST) {
                Single.fromCallable(() -> {
                    if (note.getImage() != null) {
                        boolean deleted = AppUtils.getFileFromImagesFolder(note.getImage()).getCanonicalFile().delete();
                        Log.d("TakePictureActivity", "previous image deleted: " + deleted);
                    }
                    note.setImage(mCurrentPhotoPath);
                    this.noteDao.insert(note);
                    return AppUtils.getBitmapFromImagesFolder(note.getImage());
                }).doOnSuccess(result -> {
                    // Bitmap result = (Bitmap) bundle.get("data");
                    ImageView iv = findViewById(R.id.verNotaIV);
                    iv.setImageBitmap(result);
                }).subscribe();
            }
        }
    }

    private void init() {
        this.noteDao = AppDatabase.getInstance().noteDao();
        CheckPermissionUtil.checkCamera(this, success -> Log.d("PERMISSAO PARA CAMERA",
                success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS"));
        this.recoverPutExtra();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("CheckResult")
    private void recoverPutExtra() {
        this.note = (Note) getIntent().getSerializableExtra("note");
        if (note != null && note.getImage() != null) {
            this.mCurrentPhotoPath = note.getImage();
            Single.fromCallable(() -> AppUtils.getBitmapFromImagesFolder(note.getImage()))
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                //note.setText(item.getNote());
                ImageView iv = findViewById(R.id.verNotaIV);
                iv.setImageBitmap(result);
                iv.setVisibility(View.VISIBLE);
            });
        } else {
            note = new Note();
        }
    }

    public void takePicture(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, createImageFile());
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @SuppressLint("SimpleDateFormat")
    private Uri createImageFile() {
        this.mCurrentPhotoPath = String.format("IMG_%s.jpg", new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()));
        return AppUtils.getFileURI(getApplicationContext(), AppUtils.getFileFromImagesFolder(this.mCurrentPhotoPath));
    }

    public void shareFile(View view) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "App foto");
        shareIntent.putExtra(Intent.EXTRA_STREAM, AppUtils.getFileURI(getApplicationContext(), AppUtils.getFileFromImagesFolder(this.mCurrentPhotoPath)));
        shareIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
    }

//    private Uri getImage() {
//        ContextWrapper cw = new ContextWrapper(getApplicationContext());
//        //para pegar o path de instalação to /data/data/yourapp/app_data/imageDir
//        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//    }
}
