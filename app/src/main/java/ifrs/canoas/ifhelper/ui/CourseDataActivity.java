package ifrs.canoas.ifhelper.ui;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Strings;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Note;
import ifrs.canoas.ifhelper.ui.abstracts.PrivateActivity;
import ifrs.canoas.ifhelper.ui.adapter.NotesListAdapter;
import ifrs.canoas.ifhelper.util.AppUtils;
import ifrs.canoas.ifhelper.util.CheckPermissionUtil;
import ifrs.canoas.ifhelper.viewmodel.CourseDataViewModel;

public class CourseDataActivity extends PrivateActivity {

    EditText etNote;
    TextView tvCourse;
    ListView listNotes;
    Menu menu;
    private CourseDataViewModel courseDataViewModel;
    private Integer courseId = 0;
    private String courseName = null;

    private final static int CAMERA_REQUEST = 1;//Constante para request code
    private String mCurrentPhotoPath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_curso);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckPermissionUtil.checkCamera(this, success -> Log.d("PERMISSAO PARA CAMERA",
                success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS"));
        this.init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_data_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_delete_notes) {
            this.courseDataViewModel.deleteNotes();
            return true;
        }
        if (id == R.id.menu_camera) {
            this.takePicture();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentReturn) {
        if (intentReturn != null) {
            Bundle bundle = intentReturn.getExtras();
            Log.d("Course: ", courseId + " " + requestCode + " " + mCurrentPhotoPath);
            if (bundle != null && requestCode == CAMERA_REQUEST) {
                this.courseDataViewModel.saveNote(new Note(this.courseId, this.mCurrentPhotoPath));
            }
        }
    }

    private void init() {
        this.tvCourse = findViewById(R.id.tvCourse);
        this.etNote = findViewById(R.id.etNote);
        this.listNotes = findViewById(R.id.listNotes);
        this.recoverPutExtra();
        this.tvCourse.setText(String.format(Locale.getDefault(), "%d - %s", this.courseId, this.courseName));

        this.courseDataViewModel = ViewModelProviders.of(this).get(CourseDataViewModel.class);
        this.courseDataViewModel.init(this.courseId);

        this.courseDataViewModel.getNotes().observe(this, notes -> {
            if (notes == null)
                notes = new ArrayList<>();
            Log.d("notes", Arrays.toString(notes.toArray()));
            NotesListAdapter adapter = new NotesListAdapter(notes, getApplicationContext());

            adapter.setCheckedIds(courseDataViewModel.getCheckedIds());
            adapter.setOnItemClickListener((parent, view, position, elementId) -> {
                Log.d("checked", Arrays.toString(adapter.getCheckedIds().toArray()));
                courseDataViewModel.setCheckedIds(adapter.getCheckedIds());
                menu.findItem(R.id.menu_delete_notes).setVisible(adapter.getCheckedIds().size() > 0);
            });

            adapter.setOnItemLongClickListener((parent, view, position, elementId) -> {
                Note note = ((Note) listNotes.getAdapter().getItem(position));
                if (note.getImage() != null) {
                    Intent intent = new Intent(getApplicationContext(), TakePictureActivity.class);
                    intent.putExtra("note", note);
                    startActivity(intent);
                } else {
                    courseDataViewModel.editNote(note.getId());
                }
                return false;
            });

            if (menu != null) {
                menu.findItem(R.id.menu_delete_notes)
                        .setVisible(courseDataViewModel.getCheckedIds() != null
                                && !courseDataViewModel.getCheckedIds().isEmpty());
            }

            listNotes.setAdapter(adapter);
        });

        this.courseDataViewModel.getNote().observe(this, note -> etNote.setText(note == null ? "" : note.getNote()));

        this.courseDataViewModel.getAlert().observe(this, alert -> {
            if (!Strings.isNullOrEmpty(alert)) {
                Toast.makeText(getApplicationContext(), alert, Toast.LENGTH_LONG).show();
                courseDataViewModel.getAlert().setValue("");
            }
        });

    }

    private void recoverPutExtra() {
        this.courseId = ((Long) getIntent().getLongExtra("courseId", 0)).intValue();
        this.courseName = getIntent().getStringExtra("courseName");
        Snackbar.make(getWindow().getDecorView().getRootView(), "Id do curso: " + courseId, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void saveNote(View view) {
        this.courseDataViewModel.saveNote(etNote.getText().toString());
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, createImageFile());
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @SuppressLint("SimpleDateFormat")
    private Uri createImageFile() {
        this.mCurrentPhotoPath = String.format("IMG_%s.jpg", new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()));
        return AppUtils.getFileURI(getApplicationContext(), AppUtils.getFileFromImagesFolder(this.mCurrentPhotoPath));
    }
}
