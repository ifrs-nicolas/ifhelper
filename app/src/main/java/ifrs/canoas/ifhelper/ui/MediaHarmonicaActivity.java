package ifrs.canoas.ifhelper.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.MediaHarmonicaCalculator;

public class MediaHarmonicaActivity extends AppCompatActivity {

    private static MediaHarmonicaCalculator calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_harmonica);

        this.initCalculator();
    }

    @SuppressLint("SetTextI18n")
    private void initCalculator() {
        if (MediaHarmonicaActivity.calculator == null) {
            MediaHarmonicaActivity.calculator = new MediaHarmonicaCalculator();
            EditText etLabel = findViewById(R.id.etLabel);
            etLabel.setText("N1");
        } else this.updateLabels();
    }

    protected void updateLabels() {
        EditText etLabel = findViewById(R.id.etLabel);
        etLabel.setText(String.format("N%d", calculator.getSize() + 1));

        TextView sumPesos = findViewById(R.id.sumPesos);
        sumPesos.setText(String.valueOf(calculator.getSumPesos()));

        TextView grades = findViewById(R.id.notas);
        grades.setText(calculator.getNotas());
    }

    public void addValue(View v) {
        EditText etLabel = findViewById(R.id.etLabel);
        EditText etNota = findViewById(R.id.nota);
        EditText etPeso = findViewById(R.id.peso);
        if (etNota.getText().toString().trim().isEmpty() || etPeso.getText().toString().trim().isEmpty()) {
            TextView resultado = findViewById(R.id.resultadoMH);
            resultado.setText("Todos os campos devem estar preenchidos.");
            return;
        }
        try {
           double nota = Double.valueOf(etNota.getText().toString());
           double peso = Double.valueOf(etPeso.getText().toString());
           calculator.addNota(etLabel.getText().toString(), nota, peso);
        } catch (Exception e) {
            TextView resultado = findViewById(R.id.resultadoMH);
            resultado.setText(e.getMessage());
            return;
        }

        this.updateLabels();
        etNota.setText("");
        etPeso.setText("");
        etNota.requestFocus();
    }

    public void calculaMediaHarmonica(View v) {
        TextView resultado = findViewById(R.id.resultadoMH);
        try {
            resultado.setText(String.valueOf(calculator.calcula()));
        } catch (Exception e) {
            resultado.setText(e.getMessage());
        }
    }

    public void limpaCalculos(View v) {
        calculator = new MediaHarmonicaCalculator();
        EditText etLabel = findViewById(R.id.etLabel);
        EditText etNota = findViewById(R.id.nota);
        EditText etPeso = findViewById(R.id.peso);
        TextView sumPesos = findViewById(R.id.sumPesos);
        TextView notas = findViewById(R.id.notas);
        TextView resultado = findViewById(R.id.resultadoMH);
        etLabel.setText("N1");
        etNota.getText().clear();
        etPeso.getText().clear();
        sumPesos.setText("");
        notas.setText("");
        resultado.setText("");
    }
}
