package ifrs.canoas.ifhelper.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Message;
import ifrs.canoas.ifhelper.model.MessageWrapper;
import ifrs.canoas.ifhelper.ui.abstracts.PrivateActivity;
import ifrs.canoas.ifhelper.ui.adapter.MessagesListAdapter;
import ifrs.canoas.ifhelper.util.AppUtils;
import ifrs.canoas.ifhelper.webservice.Moodle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagesListActivity extends PrivateActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_mensagem);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.verifyUser()) {
            this.getMessagesFromWS();
        }
    }

    private void getMessagesFromWS() {
        Moodle.getCoursesWS().getMessages(user.getToken(), user.getUserid()).enqueue(new Callback<MessageWrapper>() {
            public void onResponse(@NonNull Call<MessageWrapper> call, @NonNull Response<MessageWrapper> response) {
                if (response.body() != null && "invalidtoken".equals(response.body().getErrorcode())) {
                    AppUtils.removeUser();
                    verifyUser();
                } else {
                    List<Message> list = response.body() == null || response.body().getMessages() == null ? new ArrayList<>() : response.body().getMessages();
                    SharedPreferences sharedPref = getSharedPreferences("moodle_messages", Context.MODE_PRIVATE);
                    sharedPref.edit().putInt("messages_size", list.size()).apply();
                    mountList(list);
                }
            }

            public void onFailure(@NonNull Call<MessageWrapper> call, @NonNull Throwable t) {
                Log.e("Error MoodleAPI", t.getMessage());
            }
        });
    }

    private void mountList(List<Message> messages) {
        ListView list = findViewById(R.id.MensagemListView);
        list.setAdapter(new MessagesListAdapter(getApplicationContext(), messages));
    }
}
