package ifrs.canoas.ifhelper.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.stetho.Stetho;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.service.ServiceInitialize;

//Selecione a aba de todo e resolva todos eles inclusive esse
//TODO transformar essa tela no idioma Inglês e Português
//TODO adicionar um icone para cada Botão.
//TODO adicionar métodos para todas as funções da atividade principal
//TODO Adicionar remoto e submeter a atividade.(ver tutorial da aula).

//Basic activity com floatButton removido
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sendBroadcast(new Intent(this, ServiceInitialize.class).setAction("ifhelper.services.initialize"));

        Stetho.initializeWithDefaults(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void calculateAbsences(View v) {
        startActivity(new Intent(getApplicationContext(), CalculaFaltasActivity.class));
    }

    public void mySpace(View v){
        startActivity(new Intent(getApplicationContext(), CoursesListActivity.class));
    }

    public void messages(View v){
        startActivity(new Intent(getApplicationContext(), MessagesListActivity.class));
    }

    public void academicCalendar(View v){
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://ifrs.edu.br/canoas/wp-content/uploads/sites/6/2018/06/Calend%C3%A1rio-Acad%C3%AAmico-2018-atualizado-junho.pdf")));
        //startActivity(new Intent(getApplicationContext(), TakePictureActivity.class));
    }

    public void calculateHarmonicMean(View v) {
        startActivity(new Intent(getApplicationContext(), MediaHarmonicaActivity.class));
    }

    public void cleanData(View v){
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.user), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }
}
