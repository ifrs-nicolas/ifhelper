package ifrs.canoas.ifhelper.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Course;
import ifrs.canoas.ifhelper.ui.abstracts.PrivateActivity;
import ifrs.canoas.ifhelper.ui.adapter.CoursesListAdapter;
import ifrs.canoas.ifhelper.viewmodel.CoursesListViewModel;

public class CoursesListActivity extends PrivateActivity {

    private ListView lvCourses;
    private SearchView searchCourse;
    private CoursesListViewModel coursesListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_disciplinas);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.verifyUser()) {
            this.init();
        }
    }

    private void init() {
        Log.d("user", user.toString());
        this.lvCourses = findViewById(R.id.courses_list);
        this.searchCourse = findViewById(R.id.search_course);

        this.coursesListViewModel = new CoursesListViewModel();
        this.coursesListViewModel.init(user);
        this.coursesListViewModel.getCourses().observe(this,
                courses -> lvCourses.setAdapter(new CoursesListAdapter(courses, getApplicationContext())));

        this.lvCourses.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(getApplicationContext(), CourseDataActivity.class);
            intent.putExtra("courseId", id);//Observe que esse é o id lá do webservice
            intent.putExtra("courseName", ((Course) lvCourses.getItemAtPosition(position)).getShortname());
            startActivity(intent);
        });

        this.searchCourse.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d("queryText",query);
                ((CoursesListAdapter) lvCourses.getAdapter()).getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.d("queryText change: ", query);
                ((CoursesListAdapter) lvCourses.getAdapter()).getFilter().filter(query);
                return false;
            }
        });
    }

}
