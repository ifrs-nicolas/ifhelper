package ifrs.canoas.ifhelper.ui.abstracts;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.User;
import ifrs.canoas.ifhelper.ui.LoginActivity;
import ifrs.canoas.ifhelper.util.AppUtils;

public abstract class PrivateActivity extends BaseActivity {

    protected User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected boolean verifyUser() {
        this.user = AppUtils.getUser();
        if (user == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return false;
        } else return true;
    }
}
