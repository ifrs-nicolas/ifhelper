package ifrs.canoas.ifhelper.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Course;

public class CoursesListAdapter extends BaseAdapter implements Filterable {

    private ArrayList<Course> originalData;
    private Context context;
    private ArrayList<Course> filteredData;
    private ItemFilter mFilter;

    public CoursesListAdapter(List<Course> list, Context context) {
        this.originalData = list == null ? new ArrayList<>() : new ArrayList<>(list);
        this.filteredData = this.originalData;
        this.mFilter = new ItemFilter();
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return this.filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.filteredData.get(position).getId();
    }


    @Override
    @SuppressLint({"ViewHolder", "InflateParams"})
    public View getView(int position, View convertView, ViewGroup parent) {
        Course disp = this.filteredData.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = null;
        if (inflater != null) {
            layout = (LinearLayout) inflater.inflate(R.layout.item_disciplina, null);
            TextView twNome = layout.findViewById(R.id.nome_disciplina);
            twNome.setText(disp.getShortname());
            TextView twDesc = layout.findViewById(R.id.descricao_disciplina);
            twDesc.setText(disp.toString());
        }
        return layout;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if (!filterString.trim().isEmpty()) {
                final ArrayList<Course> list = originalData;

                int count = list.size();
                final ArrayList<Course> nlist = new ArrayList<>(count);

                Course filterableDisp;
                for (int i = 0; i < count; i++) {
                    filterableDisp = list.get(i);
                    if (filterableDisp.getShortname().toLowerCase().contains(filterString)
                            || filterableDisp.toString().toLowerCase().contains(filterString)) {
                        nlist.add(filterableDisp);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }
            return results;
        }

        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Course>) results.values;
            notifyDataSetChanged();
        }
    }
}
