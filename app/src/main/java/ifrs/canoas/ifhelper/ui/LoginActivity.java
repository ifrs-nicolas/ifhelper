package ifrs.canoas.ifhelper.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;

import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Token;
import ifrs.canoas.ifhelper.model.User;
import ifrs.canoas.ifhelper.util.WebServiceUtil;

public class LoginActivity extends AppCompatActivity {

    private TextView message;
    private static Token token;
    private static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portal_aluno);

        this.message = findViewById(R.id.mensagem);
        ((EditText) findViewById(R.id.senha)).setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                findViewById(R.id.buttonLogin).performClick();
                return true;
            }
            return false;
        });
    }

    public void login(View view) {
        EditText etLogin = findViewById(R.id.login);
        EditText etPassword = findViewById(R.id.senha);
        String pass = etPassword.getText().toString().isEmpty() ? "" : etPassword.getText().toString();
        Log.i("login", etLogin.getText().toString());
        String uri = "http://moodle.canoas.ifrs.edu.br/login/token.php";
        uri += "?username="+ etLogin.getText().toString() +
                "&password=" + pass  +
                "&service=moodle_mobile_app";
        new LoginWSConsumer().execute(uri);
    }

    @SuppressLint("StaticFieldLeak")
    private class LoginWSConsumer extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return WebServiceUtil.getContentAsString(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid." + e;
            }
        }
        @Override
        protected void onPostExecute(String result) {
            Log.d("moodle login", result);
            Gson g = new Gson();
            token = g.fromJson(result.trim(), Token.class);
            if (token.getError() == null)  {
                message.setText(String.format("TOKEN: %s", token.getToken()));
                getUserInfo();
            } else {
                message.setText(token.getError());
            }
        }
    }

    private void getUserInfo() {
        String uri = "http://moodle.canoas.ifrs.edu.br/webservice/rest/server.php";
        uri += "?wstoken="+ token.getToken() + "&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json";
        new UserInfoConsumer().execute(uri);
    }

    @SuppressLint("StaticFieldLeak")
    private class UserInfoConsumer extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return WebServiceUtil.getContentAsString(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid." + e;
            }
        }
        @Override
        protected void onPostExecute(String result) {
            user = new Gson().fromJson(result.trim(), User.class);
            user.setToken(token.getToken());
            saveUserInSharedPreferences();
        }
    }

    private void saveUserInSharedPreferences() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.user), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("token", user.getToken());
        editor.putInt("userId", user.getUserid());
        editor.putString("fullname", user.getFullname());
        editor.putString("username", user.getUsername());
        editor.apply();
        this.finish();
    }
}
