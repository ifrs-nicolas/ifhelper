package ifrs.canoas.ifhelper.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ifrs.canoas.ifhelper.App;
import ifrs.canoas.ifhelper.R;
import ifrs.canoas.ifhelper.model.Message;
import ifrs.canoas.ifhelper.util.AppUtils;

public class MessagesListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Message> list;

    public MessagesListAdapter(Context context, List<Message> list) {
        this.mInflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int arg0) {
        return list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return list.get(arg0).getId();
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View layout, ViewGroup arg2) {
        layout = mInflater.inflate(R.layout.item_message, null);
        TextView title = layout.findViewById(R.id.userFrom);
        final TextView description = layout.findViewById(R.id.assunto);
        final Message message = (Message) this.getItem(position);
        title.setText(message.getUserfromfullname());
        String desc = message.getSmallmessage().replaceAll("<.*?>", "");
        final String finalDesc = desc.length() > 100 ? desc.substring(0, 100) : desc;
        description.setText(finalDesc);
        final String linkURL = AppUtils.getUrlFromString(message.getFullmessage());

        Button bt = layout.findViewById(R.id.mensagemCompleteDescriptionBT);
        bt.setFocusable(false);
        bt.setFocusableInTouchMode(false);
        bt.setOnClickListener(v -> App.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(linkURL))));

        layout.setOnClickListener(v -> {
            if (message.isToogle()) {
                description.setText(finalDesc);
                message.setToogle(false);
            } else {
                description.setText(message.getFullmessage());
                message.setToogle(true);
            }
        });
        return layout;
    }
}
