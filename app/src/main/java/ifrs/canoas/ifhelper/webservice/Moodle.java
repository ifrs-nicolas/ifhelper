package ifrs.canoas.ifhelper.webservice;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Moodle {

    private static final String BASE_URL = "http://moodle.canoas.ifrs.edu.br/webservice/rest/";

    private static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    public static CoursesWS getCoursesWS() {
        return getRetrofit().create(CoursesWS.class);
    }
}
