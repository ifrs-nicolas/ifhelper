package ifrs.canoas.ifhelper.webservice;

import org.json.JSONObject;

import java.util.List;

import ifrs.canoas.ifhelper.model.Course;
import ifrs.canoas.ifhelper.model.Message;
import ifrs.canoas.ifhelper.model.MessageWrapper;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CoursesWS {

    @GET("server.php?wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json")
    Call<List<Course>> getCoursesByUser(@Query("wstoken") String token, @Query("userid") Integer userid);

    @GET("server.php?wsfunction=core_message_get_messages&type=conversations&moodlewsrestformat=json")
    Call<MessageWrapper> getMessages(@Query("wstoken") String token, @Query("useridto") Integer userid);
}
