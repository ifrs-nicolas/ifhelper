package ifrs.canoas.ifhelper.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Course {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id_moodle")
    private Integer id;
    @ColumnInfo
    private String fullname;
    @ColumnInfo
    private String shortname;
    @ColumnInfo(name = "user_id")
    private Integer userId;

    @Override
    public String toString() {
        return "ID: " + this.getId() +
                " - " + this.getFullname() +
                " - " + this.getShortname();
    }

    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }
    public String getFullname() { return fullname; }
    public void setFullname(String fullname) { this.fullname = fullname; }
    public String getShortname() { return shortname; }
    public void setShortname(String shortname) { this.shortname = shortname; }
    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
