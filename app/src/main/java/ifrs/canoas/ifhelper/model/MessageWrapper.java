package ifrs.canoas.ifhelper.model;

import java.util.List;

public class MessageWrapper extends MoodleAPI {

    private List<Message> messages;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
