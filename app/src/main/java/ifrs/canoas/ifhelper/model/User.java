package ifrs.canoas.ifhelper.model;

import java.io.Serializable;

public class User implements Serializable {

    private String token;
    private String username;
    private String fullname;
    private Integer userid;

    @Override
    public String toString() {
        return "UserInfo{" +
                "username='" + username + '\'' +
                ", fullname='" + fullname + '\'' +
                ", userid=" + userid +
                '}';
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    public Integer getUserid() {
        return userid;
    }
    public void setUserid(Integer userid) {
        this.userid = userid;
    }
}
