package ifrs.canoas.ifhelper.model;

/**
 * Created by marcio on 06/08/17.
 */

class ItemMedia {
    private String name;
    private double nota;
    private double peso;


    public ItemMedia(String name, double nota, double peso) {
        this.name = name;
        this.nota = nota;
        this.peso = peso;
    }

    public String getName() {
        return name;
    }

    public ItemMedia setName(String name) {
        this.name = name;
        return this;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getPercentual() {
        if (this.nota == 0)
            return this.peso / 0.0001;
        return this.peso / this.nota;
    }

    @Override
    public String toString() {
        return this.name + ": " + String.valueOf(this.nota) + "/" +String.valueOf(this.peso);
    }
}
