package ifrs.canoas.ifhelper.model;

import java.io.Serializable;

public class Token implements Serializable {

    private String error;
    private String token;

    @Override
    public String toString() {
        return "User{" +
                "token='" + token + '\'' +
                '}';
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}