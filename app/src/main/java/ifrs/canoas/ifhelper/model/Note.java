package ifrs.canoas.ifhelper.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Note implements Serializable {

    @PrimaryKey
    private Integer id;
    @ColumnInfo
    private String note;
    @ColumnInfo(name = "course_id")
    private Integer courseId;
    @ColumnInfo(name = "created_date")
    private Date createdDate;
    @ColumnInfo
    private String image;
    @Ignore
    private transient Bitmap bmImage;

    public Note() {}

    @Ignore
    public Note(Integer courseId, String image) {
        this.courseId = courseId;
        this.image = image;
        this.createdDate = Calendar.getInstance().getTime();
    }

    @Override
    public String toString() {
        return note; //+ " \n" + AppUtils.brDate(createdDate);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Bitmap getBmImage() {
        return bmImage;
    }

    public void setBmImage(Bitmap bmImage) {
        this.bmImage = bmImage;
    }
}
