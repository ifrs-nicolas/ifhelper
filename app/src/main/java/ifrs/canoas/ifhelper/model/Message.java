package ifrs.canoas.ifhelper.model;

import android.annotation.SuppressLint;

public class Message {

    private int id;
    private String userfromfullname;
    private String smallmessage;
    private int useridfrom;
    private String subject;
    private String fullmessage;
    //private int timecreated;
    //private int timeread;

    private boolean toogle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserfromfullname() {
        return userfromfullname;
    }

    public void setUserfromfullname(String userfromfullname) {
        this.userfromfullname = userfromfullname;
    }

    public String getSmallmessage() {
        return smallmessage;
    }

    public void setSmallmessage(String smallmessage) {
        this.smallmessage = smallmessage;
    }

    public int getUseridfrom() {
        return useridfrom;
    }

    public void setUseridfrom(int useridfrom) {
        this.useridfrom = useridfrom;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFullmessage() {
        return fullmessage;
    }

    public void setFullmessage(String fullmessage) {
        this.fullmessage = fullmessage;
    }

    public boolean isToogle() {
        return toogle;
    }

    public void setToogle(boolean toogle) {
        this.toogle = toogle;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public String toString() {
        return String.format("Message{id=%d, userfromfullname='%s', smallmessage='%s', useridfrom=%d, subject='%s', fullmessage='%s'}",
                id, userfromfullname, smallmessage, useridfrom, subject, fullmessage);
    }
}
