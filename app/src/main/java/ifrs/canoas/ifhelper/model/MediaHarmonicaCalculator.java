package ifrs.canoas.ifhelper.model;

import java.util.ArrayList;

/**
 * Created by marcio on 06/08/17.
 */

public class MediaHarmonicaCalculator {
    private ArrayList<ItemMedia> notas = new ArrayList<>();

    public void addNota(String label, double nota, double peso) throws Exception {
        //TODO adicionar no arrayList
        double pesoRestante =  (10 - this.getSumPesos());
        if (peso == 0) {
            throw new Exception("O peso informado não pode ser igual a zero.");
        } else if (peso > pesoRestante) {
            throw new Exception("O peso informado não pode ser maior que " + pesoRestante);
        }
        this.notas.add(new ItemMedia(label, nota, peso));
    }

    /**
     * Fórmula para calculo
     * MH = 10/ (p1/n1)+(p2/n2)+..+(pn/nn)
     * onde 10 = p1+p2+...+pn
     * <p>
     * TODO verificar médias >0 ou 0,0001
     * TODO verificar se a soma dos pesos é igual a 10
     * TODO calcular
     *
     */
    public double calcula() throws Exception {
        double media = 0;
        if (notas.size() >= 2) {
            double sumPesos = this.getSumPesos();
            if (sumPesos != 10) {
                throw new Exception("A soma dos pesos não é igual a 10. Somatório dos pesos: " + sumPesos);
            }
            for (int i = 0; i < this.notas.size(); i++) {
                media += this.notas.get(i).getPercentual();
            }
            return sumPesos / media;
        } else {
            throw new Exception("Quantidade de notas insuficientes");
        }
    }

    public double getSumPesos() {
        double sumPesos = 0;
        for (int i = 0; i < this.notas.size(); i++) {
            sumPesos += this.notas.get(i).getPeso();
        }
        return sumPesos;
    }

    public String getNotas() {
        return this.notas.toString();
    }

    public int getSize() {
        return this.notas.size();
    }
}
